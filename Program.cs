﻿using System;
using FileSystemManager.Services;
using FileSystemManager.UserInteraction;

namespace FileSystemManager
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "File System Manager";
            AppMenu.ShowMenu();
        }
    }
}
