﻿# File System Manager

File System Manager is a command line application for reading and manipulating files

## Installation

Clone this repo and run the solution

## Usage

The application presents the user with a menu of the different operations that are available to be executed.

Choose a menu option and view the results in the console.

The results of each operation along with its exectuion time will also be logged to a Log.txt file in the root directory of the project.
