﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace FileSystemManager.Services
{
    /*This class has the sole responsibility of returning information about files in the Resources folder
     All logging and console printing functionality is handled by the LoggingService class*/
    class FileService

    {
        private static readonly string resourcesFilepath = @"..\..\..\Resources";
        public static string[] GetFileNames()
        {
            string[] fileNames = Directory.GetFiles(resourcesFilepath)
                        .Select(Path.GetFileName)
                        .ToArray();
            return fileNames;
        }

        public static string[] GetFileExtensions()
        {
            /*Here i used a HashSet since it can only contain unique values. This way the extensions will not be duplicated.
            I also used the Lambda Syntax of LINQ to get the files in a short and concise way.*/
            HashSet<string> extensionsHashSet = new HashSet<string>();
            extensionsHashSet = Directory.GetFiles(resourcesFilepath)
                       .Select(Path.GetExtension)
                       .ToHashSet();
            return extensionsHashSet.ToArray();
        }

        public static string[] GetFilesByExtension(string extension)
        {
            string[] resourceFiles = Directory.GetFiles(resourcesFilepath, $"*{extension}")
                        .Select(Path.GetFileName)
                        .ToArray();
            return resourceFiles;
        }

        public static string GetDraculaFileName()
        {
            return Path.GetFileName(@$"{resourcesFilepath}\Dracula.txt");
        }

        public static long GetDraculaFileSize()
        {
            return new FileInfo(@$"{resourcesFilepath}\Dracula.txt").Length;
        }

        public static int GetDraculaFileLineCount()
        {
            int lineCount = 0;
            using var reader = new StreamReader($"{resourcesFilepath}\\Dracula.txt");
            while (reader.ReadLine() != null)
            {
                lineCount++;
            }
            return lineCount;
        }

        /*This method uses regex to make sure that the word to be found is not part of another word, but it does get found
         if the word ends with a special character*/
        public static int GetDraculaFileWordCount(string word)
        {
            int wordCount = 0;
            Regex wholeWord = new Regex($@"\b{word}\b", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            using (StreamReader rdr = new StreamReader($"{resourcesFilepath}\\Dracula.txt"))
            {
                string line;
                while ((line = rdr.ReadLine()) != null)
                {
                    wordCount += wholeWord.Matches(line).Count;
                }
            }
            return wordCount;
        }
    }
}
