﻿using System;
using System.Diagnostics;
using System.IO;

namespace FileSystemManager.Services
{
    /*This class is responsible for logging as well as timing each function from the FileService class.
     Since every method has a different log output, there is one method corresponding to each FileService Method*/
    class LoggingService
    {
        private static readonly string logFilepath = "..\\..\\..\\Log.txt";

        public static void LogTextToFile(string functionOutput)
        {
            var currentTime = DateTime.Now.ToString("dd'-'MM'-'yyyy'T'HH':'mm':'ss");
            string logMessage = $"{currentTime}: {functionOutput}";
            try
            {

                File.AppendAllText(@logFilepath, logMessage + Environment.NewLine);
            }
            catch (Exception ex)
            {
                Console.WriteLine("error: " + ex);
            }
        }

        public static void LogFileNames()
        {
            var sw = Stopwatch.StartNew();
            string[] fileNames = FileService.GetFileNames();
            sw.Stop();
            double executionTime = sw.Elapsed.TotalMilliseconds;
            string fileNamesString = String.Join(", ", fileNames);
            string message = $"Directory contains following files: {fileNamesString}";
            Console.WriteLine(message);
            string logText = $"{message}. The function took {executionTime} ms to Execute";
            LogTextToFile(logText);
        }

        public static void LogFileExtensions()
        {
            var sw = Stopwatch.StartNew();
            string[] fileExtensions = FileService.GetFileExtensions();
            sw.Stop();
            double executionTime = sw.Elapsed.TotalMilliseconds;
            string fileExtensionsString = String.Join(", ", fileExtensions);
            string message = $"Directory contains following file types: {fileExtensionsString}";
            Console.WriteLine(message);
            string logText = $"{message}. The function took {executionTime} ms to Execute";
            LogTextToFile(logText);
        }

        public static void LogFilesByExtension(string extension)
        {
            var sw = Stopwatch.StartNew();
            string[] fileNames = FileService.GetFilesByExtension(extension);
            sw.Stop();
            double executionTime = sw.Elapsed.TotalMilliseconds;
            string fileNamesString = String.Join(", ", fileNames);
            string message = fileNamesString.Length == 0 ? $"Directory does not contain any files with extension {extension}"
                : $"Directory contains following files with extension {extension}: {fileNamesString}";
            Console.WriteLine(message);
            string logText = $"{message}. The function took {executionTime} ms to Execute";
            LogTextToFile(logText);
        }

        public static void LogDraculaFileName()
        {
            var sw = Stopwatch.StartNew();
            string fileName = FileService.GetDraculaFileName();
            sw.Stop();
            double executionTime = sw.Elapsed.TotalMilliseconds;
            string message = $"Filename: {fileName}";
            Console.WriteLine(message);
            string logText = $"{message}. The function took {executionTime} ms to Execute";
            LogTextToFile(logText);
        }

        public static void LogDraculaFileSize()
        {
            var sw = Stopwatch.StartNew();
            long fileSize = FileService.GetDraculaFileSize();
            sw.Stop();
            double executionTime = sw.Elapsed.TotalMilliseconds;
            string message = $"File size: {fileSize / 1000}KB";
            Console.WriteLine(message);
            string logText = $"{message}. The function took {executionTime} ms to Execute";
            LogTextToFile(logText);
        }

        public static void LogDraculaFileLineCount()
        {
            var sw = Stopwatch.StartNew();
            int lineCount = FileService.GetDraculaFileLineCount();
            sw.Stop();
            double executionTime = sw.Elapsed.TotalMilliseconds;
            string message = $"There are {lineCount} lines in the file";
            Console.WriteLine(message);
            string logText = $"{message}. The function took {executionTime} ms to Execute";
            LogTextToFile(logText);
        }


        public static void LogDraculaFileWordCount(string word)
        {
            var sw = Stopwatch.StartNew();
            int wordCount = FileService.GetDraculaFileWordCount(word);
            sw.Stop();
            double executionTime = sw.Elapsed.TotalMilliseconds;
            string message = wordCount == 0 ? "This word does not appear in the text"
                : $"The word {word} appears {wordCount} times in the text";
            Console.WriteLine(message);
            string logText = $"{message}. The function took {executionTime} ms to Execute";
            LogTextToFile(logText);
        }

        //public static void Invoke(Action action, out long executionTime)
        //{
        //    var sw = Stopwatch.StartNew();
        //    action.Invoke();
        //    sw.Stop();
        //    executionTime = sw.ElapsedMilliseconds;
        //}

    }

}
