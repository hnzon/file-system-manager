﻿using System;
using FileSystemManager.Services;

namespace FileSystemManager.UserInteraction
{
    class AppMenu
    {
        public static void ShowMenu()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("File System Manager");
            Console.ResetColor();
            Console.WriteLine("Choose an option:");
            Console.WriteLine("1. List all files in Resources directory");
            Console.WriteLine("2. List all files of a certain type in Resources directory");
            Console.WriteLine("3. Show filename of Dracula file");
            Console.WriteLine("4. Show filesize of Dracula file");
            Console.WriteLine("5. Check how many lines the Dracula file has");
            Console.WriteLine("6. Check how many times a word occurs in the Dracula file");
            Console.WriteLine("7. Exit");

            string userInput = Console.ReadLine();
            Console.WriteLine();
            HandleInput(userInput);
            Console.WriteLine();
            GoBackPrompt();
        }

        private static void HandleInput(string userInput)
        {
            switch (userInput)
            {
                case "1":
                    LoggingService.LogFileNames();
                    break;
                case "2":
                    LoggingService.LogFileExtensions();
                    Console.WriteLine("Enter a file extension");
                    string ext = Console.ReadLine();
                    LoggingService.LogFilesByExtension(ext);
                    break;
                case "3":
                    LoggingService.LogDraculaFileName();
                    break;
                case "4":
                    ;
                    LoggingService.LogDraculaFileSize();
                    break;
                case "5":
                    LoggingService.LogDraculaFileLineCount();
                    break;
                case "6":
                    Console.WriteLine("What word would you like to search for?");
                    string word = Console.ReadLine();
                    LoggingService.LogDraculaFileWordCount(word);
                    break;
                case "7":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine("Invalid input");
                    ShowMenu();
                    break;
            }
        }

        //After every operation you can choose to go back to menu or exit
        public static void GoBackPrompt()
        {
            Console.WriteLine("1. Go back to menu   2. Exit");
            string userInput = Console.ReadLine();
            switch (userInput)
            {
                case "1":
                    Console.WriteLine();
                    ShowMenu();
                    break;
                case "2":
                    Environment.Exit(0);
                    break;
                default:
                    Console.WriteLine();
                    Console.WriteLine("Invalid input");
                    GoBackPrompt();
                    break;
            }
        }
    }
}
